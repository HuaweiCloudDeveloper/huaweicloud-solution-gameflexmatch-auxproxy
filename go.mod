module codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy

go 1.16

require (
	github.com/beego/beego/v2 v2.0.7
	github.com/huaweicloud/huaweicloud-sdk-go-obs v3.22.11+incompatible
	github.com/mitchellh/go-ps v1.0.0
	github.com/pkg/errors v0.9.1
	github.com/smartystreets/goconvey v1.7.2
	github.com/stretchr/testify v1.8.2
	go.uber.org/zap v1.24.0
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
