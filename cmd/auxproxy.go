// Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

// auxproxy启动入口
package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/restorer"

	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/config"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/buildmanager"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/configmanager"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/dnsmanager"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/grpcserver"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/httpserver"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/processmanager"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/clients"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/hhmac"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/log"
)

func init() {
	flag.StringVar(&config.Opts.CloudPlatformAddr, "cloud-platform-address",
		"", "cloud platform address for user data and meta data")
	flag.StringVar(&config.Opts.AuxProxyAddr, "auxproxy-address", "", "auxproxy address")
	flag.StringVar(&config.Opts.GrpcAddr, "grpc-address", "",
		"grpc address, this should listen at localhost, or we can not get right peer ip")
	flag.StringVar(&config.Opts.GatewayAddr, "gateway-address", "", "application gateway address")
	flag.StringVar(&config.Opts.CmdFleetId, "cmd-fleet-id", "", "fleet id from cmd")
	flag.StringVar(&config.Opts.ScalingGroupId, "scaling-group-id", "", "scaling group id from cmd")

	flag.StringVar(&config.Opts.LogLevel, "log-level", config.LogLevelInfo, "log level, support "+
		"debug and info")
	flag.BoolVar(&config.Opts.EnableBuild, "enable-build-download", false, "build download enable")
	flag.BoolVar(&config.Opts.EnableTest, "enable-test-mode", false, "test enable")
	flag.StringVar(&config.Opts.GCMKey, "gcm-key", "", "gcm decode or encode key")
	flag.StringVar(&config.Opts.GCMNonce, "gcm-nonce", "", "gcm decode or encode nonce")
	flag.StringVar(&config.Opts.InitMode, "INSTANCE_TYPE", "", "POD or VM")
}

// ReturnErr return when err is not nil
func ReturnErr(err error) {
	if err != nil {
		fmt.Printf("auxproxy init failed for %v\n", err)
		os.Exit(1)
	}
}

func main() {
	flag.Parse()
	ReturnErr(log.InitLog())
	if config.Opts.EnableTest {
		log.RunLogger.Infof("Start auxproxy success")
		return
	}
	ReturnErr(log.RewriteStderrFile())
	ReturnErr(config.Init())
	ReturnErr(hhmac.InitHMACKey())
	// 如果命令行启用了应用下载功能，下载完成后直接结束进程
	if config.Opts.EnableBuild {
		ReturnErr(buildmanager.Init())
		return
	}

	// 1. init process manager
	processmanager.InitProcessManager()

	// 2. split auxproxy address and port
	_, p, err := net.SplitHostPort(config.Opts.AuxProxyAddr)
	if err != nil {
		log.RunLogger.Errorf("[main] invalid auxproxy addr %s", config.Opts.AuxProxyAddr)
		return
	}
	log.RunLogger.Infof("auxproxyaddr %s, port %s", config.Opts.AuxProxyAddr, p)

	//3. 根据模式不同选择不同的参数配置方式  mode为pod，为容器化加载模式， 选择通过appG初始化参数;否则采用原有的参数配置方式
	err = getConfig() // 拿到IP  FleetID ScalingGroupID InstanceID PublicIP PrivateIP
	if err != nil {
		log.RunLogger.Errorf("[auxproxy main] failed to get VM instance or POD instance config , err : %v", err)
		return
	}
	// 5. let config manager to get runtime config
	configmanager.ConfMgr.Work()

	// wait for config manager to get config
	<-configmanager.ConfMgr.ConfigGetChan

	err = checkDomainName(configmanager.ConfMgr.Config.InstanceConfig.IsUseDomainName)
	if err != nil {
		log.RunLogger.Errorf("Invalid domain name , err : %v", err)
		return
	}

	// 6. init and start servers
	err = InitServers()
	if err != nil {
		log.RunLogger.Errorf("failed to init server , err : %v", err)
		return
	}
	// 7. let process manager to start process
	restorer.Work()
	processmanager.ProcessMgr.Work()

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c

}

func InitServers() error {
	// init grpc server
	grpcserver.InitGrpcServer(configmanager.ConfMgr.Config.FleetID, configmanager.ConfMgr.Config.InstanceID,
		config.Opts.GrpcAddr)
	err := grpcserver.GServer.Work()
	if err != nil {
		return err
	}
	// init http server
	httpserver.InitHttpServer(config.Opts.AuxProxyAddr)
	err = httpserver.Server.Work()
    if err != nil {
        return err
    }
	return nil
}

func checkDomainName(isUseDomainName bool) error {
	if !isUseDomainName {
		return nil
	}
	err := dnsmanager.GetDomainNameConfig() // 获取域名配置信息
	if err != nil {
		log.RunLogger.Errorf("parse domainName:%s error:%+v", configmanager.DomainNameInfo.DomainName,err)
		return err
	}
	if !configmanager.DomainNameInfo.IsCheck {
		log.RunLogger.Errorf("failed to parse domainName:%s", configmanager.DomainNameInfo.DomainName)
		return fmt.Errorf("failed to parse domainName: %s ", configmanager.DomainNameInfo.DomainName)
	}
	return nil
}

func getConfig() error {
	//获取环境变量：mode，hostname，gatewayaddress
	var mode string
	if config.Opts.InitMode != "" {
		mode = config.Opts.InitMode
	}
	mode = os.Getenv("INSTANCE_TYPE")
	hostname := os.Getenv("HOSTNAME")
	gatewayaddress := os.Getenv("GATEWAYADDRESS")
	if mode == "POD" {
		// init gateway client
		clients.InitGatewayClient(gatewayaddress)

		//  init config manager
		err := configmanager.InitConfigManagerFromAppG(60*time.Second, hostname, gatewayaddress) // default config fetch interval
		if err != nil {
			log.RunLogger.Errorf("[main] failed to init config manager for %v", err)
			return err
		}

	} else {
		// init config manager
		err := configmanager.InitConfigManager(60 * time.Second) // default config fetch interval

		if err != nil {
			log.RunLogger.Errorf("[main] failed to init config manager for %v", err)
			return err
		}
		//  init gateway client
		clients.InitGatewayClient(configmanager.ConfMgr.GatewayAddr)
	}
	return nil
}