// Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

// 日志模块
package log

import (
	"fmt"
	"os"
	"runtime"
	"syscall"

	"github.com/beego/beego/v2/server/web/context"
	"go.uber.org/zap"

	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/config"
)

var (
	RunLogger *FMLogger
)

// 捕获panic日志
var StdErrFileHandler *os.File

// GetTraceLogger get trace logger from context
func GetTraceLogger(ctx *context.Context) *FMLogger {
	if tl := ctx.Input.GetData(TraceLogger); tl != nil {
		if tLogger, ok := tl.(*FMLogger); ok {
			return tLogger
		}
	}

	return RunLogger
}

// InitLog init loggers
func InitLog() error {
	atomicLevel := zap.NewAtomicLevel()
	if config.Opts.LogLevel == config.LogLevelDebug {
		atomicLevel.SetLevel(zap.DebugLevel)
	}

	var err error
	RunLogger, err = initLogger(config.RunLoggerPath, &atomicLevel)
	if err != nil {
		return fmt.Errorf("init run logger error %v", err)
	}
	RunLogger.Infof("[logger] success to init run logger")
	return nil
}

// catch panic
func RewriteStderrFile() error {
    if runtime.GOOS == "windows" {
		return nil
  	}
	file, err := os.OpenFile(outputPathDir + "/fatal.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	StdErrFileHandler = file //把文件句柄保存到全局变量，避免被GC回收

	if err = syscall.Dup2(int(file.Fd()), int(os.Stderr.Fd())); err != nil {
		return err
	}
	// 内存回收前关闭文件描述符
	runtime.SetFinalizer(StdErrFileHandler, func(fd *os.File) {
		err := fd.Close()
		if err != nil {
			return
		}
	})
	RunLogger.Infof("[logger] success to init catching painc log")

	return nil
}
