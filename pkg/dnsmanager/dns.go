package dnsmanager

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/common"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/configmanager"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/clients"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/log"
)

func CheckDomainName(isUseDomainName bool) error {
	if !isUseDomainName {
		return nil
	}
	err := GetDomainNameConfig() // 获取域名配置信息
	if err != nil {
		return err
	}
	if !configmanager.DomainNameInfo.IsCheck {
		log.RunLogger.Errorf("failed to parse domainName:%s", configmanager.DomainNameInfo.DomainName)
		return fmt.Errorf("failed to parse domainName: %s ", configmanager.DomainNameInfo.DomainName)
	}
	return nil
}

func GetDomainNameConfig() error {
	var err error
	// 获取MetaData
	configToAppG := configmanager.IPConfigToAppG{
		PublicIP:       configmanager.ConfMgr.Config.PublicIP,
		ScalingGroupID: configmanager.ConfMgr.Config.ScalingGroupID,
		InstanceID:     configmanager.ConfMgr.Config.InstanceID,
	}
	var domainNameConf configmanager.DomainNameConfig
	domainNameConf, err = PostIpAndMetadataToAppGateway(configToAppG)
	if err != nil {
		log.RunLogger.Infof("getDomainNameConfig failed from AppGateway, err:%v", err)
		return err
	}
	// 阻塞等待是为了防止NX Domain 创建前读的问题
	time.Sleep(common.DefaultSecondlBeforeParseDomainName * time.Second)
	if err := SendCheckDomainToAPPGW(configmanager.ConfMgr.Config.PublicIP, domainNameConf.DomainName); err != nil {
		log.RunLogger.Errorf("check domain name %s failed:%+v", domainNameConf.DomainName, err)
		return err
	}
	// 保存在全局变量中
	configmanager.DomainNameInfo = &configmanager.DomainNameConfig{
		DomainName: domainNameConf.DomainName,
		IsCheck:    true,
	}
	return nil
}

func PostIpAndMetadataToAppGateway(configToAppG configmanager.IPConfigToAppG) (domainNameConf configmanager.DomainNameConfig, retErr error) {
	reqBody, err := json.Marshal(configToAppG)
	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to marshal iPAndMetadata's request , err: %s", err)
		retErr = err
		return
	}
	cli := clients.NewHttpsClientWithoutCerts()
	req, err := clients.NewRequest("POST",
		fmt.Sprintf("https://%s/v1/post-ipConfigInfo", configmanager.ConfMgr.GatewayAddr),
		map[string][]string{}, bytes.NewReader(reqBody))
	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to create a iPAndMetadata's request , err: %s", err)
		retErr = err
		return
	}
	var code int
	var buf []byte
	code, buf, _, err = clients.DoRequest(cli, req)
	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to post IP and Metadata request to AppGateway, error %s", err)
		retErr = err
		return
	}
	if code != http.StatusOK {
		log.RunLogger.Errorf("[gateway client] failed to post IP and Metadata request to AppGateway, "+
			"status code is %d", code)
		retErr = fmt.Errorf("expected status code %d, get status code %d", http.StatusOK, code)
		return
	}
	err = json.Unmarshal(buf, &domainNameConf)
	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to unmarshal DomainNameConfig response for %v", err)
		retErr = err
		return
	}
	log.RunLogger.Errorf("getDomainNameConfig success :%+v", domainNameConf)
	return
}

func SendCheckDomainToAPPGW(localIP, domainName string) error {
	cli := clients.NewHttpsClientWithoutCerts()
	req, err := clients.NewRequest("GET", fmt.Sprintf("https://%s/v1/check-domain", configmanager.ConfMgr.GatewayAddr), map[string][]string{}, nil)
	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to create a check domain request , err: %s", err)
		return err
	}
	q := req.URL.Query()
	q.Add("public_ip", localIP)
	q.Add("domain_name", domainName)
	req.URL.RawQuery = q.Encode()
	var code int
	code, _, _, err = clients.DoRequest(cli, req)

	if err != nil {
		log.RunLogger.Errorf("[gateway client] failed to send check domain request to AppGateway, error %s", err)
		return err
	}
	if code != http.StatusOK {
		log.RunLogger.Errorf("[gateway client] failed to post IP and Metadata request to AppGateway, "+
			"status code is %d", code)
		return fmt.Errorf("expected status code %d, get status code %d", http.StatusOK, code)
	}
	return nil
}
