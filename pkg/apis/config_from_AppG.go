package apis

type ConfigFromAppG struct {
	FleetID        string `json:"fleet_id"`
	ScalingGroupID string `json:"scaling_group_id"`
	InstanceID     string `json:"instance_id"`
	PublicIP       string `json:"public_ip"`
	PrivateIP      string `json:"private_ip"`
	ProjectID      string `json:"project_id"`
}

type ConfigFromAppGResponse struct {
	ConfigFromAppG
}
