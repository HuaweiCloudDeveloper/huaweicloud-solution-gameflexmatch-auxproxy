// Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

// 配置管理与初始化
package configmanager

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"runtime"
	"sync"
	"time"

	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/config"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/apis"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/clients"
	"codehub-g.huawei.com/videocloud/mediaprocesscenter/auxproxy/pkg/utils/log"
)

const (
	publicIPType  = "public"
	privateIPType = "private"
)

const (
	MaxRetry         = 10
	WaitTimeInterval = 2
)

type Config struct {
	FleetID        string
	ScalingGroupID string
	InstanceID     string
	PublicIP       string
	PrivateIP      string
	ProjectID      string
	InstanceConfig apis.InstanceConfiguration
}

type ConfigManager struct {
	Interval     time.Duration
	GatewayAddr  string
	AuxProxyPort int

	ConfigGetChan chan bool
	Config        Config

	StopChan chan int
}

type Meta struct {
	// 应用下载使用的metadata
	BuildID              string `json:"build_id"`
	Bucket               string `json:"bucket"`
	Object               string `json:"object"`
	Region               string `json:"region"`
	GlobalServiceAddress string `json:"global_service_address"`
	Ak                   string `json:"ak"`
	Sk                   string `json:"sk"`
	// 弹缩使用的metadata
	FleetID        string `json:"fleet_id"`
	GatewayAddress string `json:"gateway_address"`
	ScalingGroupID string `json:"scaling_group_id"`
}

type Metadata struct {
	InstanceType string `json:"instance_type"`
	Meta         Meta   `json:"meta"`
}

type configFromAppG struct {
	FleetID        string `json:"fleet_id"`
	ScalingGroupID string `json:"scaling_group_id"`
	InstanceID     string `json:"instance_id"`
	PublicIP       string `json:"public_ip"`
	PrivateIP      string `json:"private_ip"`
	ProjectID      string `json:"project_id"`
}

type IPConfigToAppG struct {
	PublicIP       string `json:"public_ip"`
	ScalingGroupID string `json:"scaling_group_id"`
	InstanceID     string `json:"instance_id"`
}

type DomainNameConfig struct {
	DomainName string `json:"domain_name"`
	IsCheck    bool   `json:"is_check"`
}

var DomainNameInfo *DomainNameConfig

var ConfMgr *ConfigManager

// InitConfigManager init config manager
func InitConfigManager(interval time.Duration) (retErr error) {
	once := sync.Once{}
	once.Do(func() {
		// get fleet id
		var fleetID, gwAddr, sgID, err = getFleetIDAndGWAddrAndScalingGroupID()
		if err != nil {
			retErr = fmt.Errorf("[config manager] failed to get fleet id, gateway address, sg id for %v", err)
			return
		}
		if config.Opts.GatewayAddr != "" {
			gwAddr = config.Opts.GatewayAddr
		}

		if config.Opts.CmdFleetId != "" {
			fleetID = config.Opts.CmdFleetId
		}

		if config.Opts.ScalingGroupId != "" {
			sgID = config.Opts.ScalingGroupId
		}

		var instanceID, publicIP, privateIP string
		var errNew error
		for retry := 0; retry < MaxRetry; retry++ {
			instanceID, publicIP, privateIP, errNew = parseParam()
			if errNew != nil {
				retErr = err
				log.RunLogger.Errorf("get param error: %s", errNew.Error())
				return
			}
			// IP未创建成功时，查询请求能正确返回，但IP内容为空，此时再重复查询
			if errNew := checkParam(instanceID, publicIP, privateIP); errNew != nil {
				log.RunLogger.Errorf("invalid param, %s, retry: %d,", errNew.Error(), retry)
				time.Sleep(1 * time.Second)
			} else {
				break
			}
		}
		// 重复查询之后仍未得到有效id或ip，返回 error
		if err := checkParam(instanceID, publicIP, privateIP); err != nil {
			retErr = err
			log.RunLogger.Errorf("param invalid, %s", err.Error())
			return
		}
		log.RunLogger.Infof("get params success, instanceID:[%s], publidIP:[%s], privateIP:[%s]",
			instanceID, publicIP, privateIP)

		ConfMgr = &ConfigManager{
			Interval:      interval,
			GatewayAddr:   gwAddr,
			AuxProxyPort:  config.HttpsPort,
			ConfigGetChan: make(chan bool),
			Config: Config{
				FleetID:        fleetID,
				ScalingGroupID: sgID,
				InstanceID:     instanceID,
				PublicIP:       publicIP,
				PrivateIP:      privateIP,
				InstanceConfig: apis.InstanceConfiguration{},
			},
			StopChan: make(chan int),
		}
	})

	return
}

// InitConfigManagerFromAppG init config manager, get params from appG
func InitConfigManagerFromAppG(interval time.Duration, hostname string, gatewayaddr string) (retErr error) {
	once := sync.Once{}
	once.Do(func() {
		// get fleet id, sg id, instance id, public ip, private ip and project id from appGateway
		configsFromAppG, err := GetConfigFromAppGateway(hostname)
		if err != nil {
			retErr = fmt.Errorf("[config manager] failed to get fleet id, sg id, instance id, public ip, private ip for %v", err)
			log.RunLogger.Errorf("[%s] get params error: %s", hostname, err.Error())
			return
		}

		if config.Opts.CmdFleetId != "" {
			configsFromAppG.FleetID = config.Opts.CmdFleetId
		}

		if config.Opts.ScalingGroupId != "" {
			configsFromAppG.ScalingGroupID = config.Opts.ScalingGroupId
		}

		// 校验instance id 和 ip是否为有效地址
		if err := checkParam(configsFromAppG.InstanceID, configsFromAppG.PublicIP, configsFromAppG.PrivateIP); err != nil {
			retErr = err
			log.RunLogger.Errorf("param invalid, %s", err.Error())
			return
		}
		log.RunLogger.Infof("[%s] get params success, fleetID:[%s], scaling groupID:[%s], instanceID:[%s], publidIP:[%s], privateIP:[%s], projectID:[%s]",
			hostname, configsFromAppG.FleetID, configsFromAppG.ScalingGroupID, configsFromAppG.InstanceID, configsFromAppG.PublicIP, configsFromAppG.PrivateIP, configsFromAppG.ProjectID)

		ConfMgr = &ConfigManager{
			Interval:      interval,
			GatewayAddr:   gatewayaddr,
			AuxProxyPort:  config.HttpsPort,
			ConfigGetChan: make(chan bool),
			Config: Config{
				FleetID:        configsFromAppG.FleetID,
				ScalingGroupID: configsFromAppG.ScalingGroupID,
				InstanceID:     configsFromAppG.InstanceID,
				PublicIP:       configsFromAppG.PublicIP,
				PrivateIP:      configsFromAppG.PrivateIP,
				ProjectID:      configsFromAppG.ProjectID,
				InstanceConfig: apis.InstanceConfiguration{},
			},
			StopChan: make(chan int),
		}
	})

	return
}

func parseParam() (string, string, string, error) {
	// get instance id
	instanceID, err := getInstanceID()
	if err != nil {
		return "", "", "", fmt.Errorf("[config manager] failed to get instance id for %v", err)
	}
	log.RunLogger.Infof("get instanceID [%s] success", instanceID)
	// get public ip
	publicIP, err := getPublicIPOrPrivateIP(publicIPType)
	if err != nil {
		return "", "", "", fmt.Errorf("[config manager] failed to get public ip for %v", err)
	}
	log.RunLogger.Infof("get publicIP [%s] success", publicIP)
	// get private ip
	privateIP, err := getPublicIPOrPrivateIP(privateIPType)
	if err != nil {
		return "", "", "", fmt.Errorf("[config manager] failed to get private ip for %v", err)
	}
	log.RunLogger.Infof("get privateIP [%s] success", privateIP)
	return instanceID, publicIP, privateIP, nil
}

// 校验instancId ，公网IP，私网IP
func checkParam(instanceId, publicIP, privateIP string) error {
	if len(instanceId) == 0 {
		return fmt.Errorf("instanceId [%s] is invalid", instanceId)
	}
	ip_public := net.ParseIP(publicIP)
	if ip_public == nil {
		return fmt.Errorf("publicIP [%s] is invalid", publicIP)
	}

	ip_private := net.ParseIP(privateIP)
	if ip_private == nil {
		return fmt.Errorf("privateIP [%s] is invalid", publicIP)
	}
	return nil
}

// Stop stops config work
func (c *ConfigManager) Stop() {
	c.StopChan <- 1
}

// Work let config manager work
func (c *ConfigManager) Work() {
	go c.work()
}

func (c *ConfigManager) work() {
	ticker := time.NewTicker(c.Interval)

	c.fetchConfiguration()
	c.ConfigGetChan <- true

	for {
		select {
		case <-ticker.C:
			c.fetchConfiguration()
		case <-c.StopChan:
			log.RunLogger.Infof("[config manager] get config stopped")
			return
		}
	}
}

func (c *ConfigManager) fetchConfiguration() {
	log.RunLogger.Infof("[config manager] time to check configuration (every %v)", c.Interval)
	configuration, err := clients.GWClient.FetchConfiguration(ConfMgr.Config.ScalingGroupID)
	if err != nil {
		log.RunLogger.Errorf("fetch configuration from gateway failed, err %v", err)
		return
	}
	c.Config.InstanceConfig = *configuration
	log.RunLogger.Infof("[config manager] has config %+v", c)
}

func getFleetIDAndGWAddrAndScalingGroupID() (string, string, string, error) {
	if runtime.GOOS == "windows" {
		return getFleetIDAndGWAddrAndScalingGroupIDFromFile()
	}

	cli := clients.NewHttpsClientWithoutCerts()

	req, err := clients.NewRequest("GET",
		fmt.Sprintf("%s/openstack/latest/user_data", config.Opts.CloudPlatformAddr),
		map[string][]string{}, nil)
	if err != nil {
		return "", "", "", err
	}

	var code int
	var buf []byte
	reqOK := false
	// 获取user_data参数，失败每隔2s重试一次，最多10次
	for i := 0; i < MaxRetry && reqOK == false; i++ {
		code, buf, _, err = clients.DoRequest(cli, req)
		if err != nil || code != http.StatusOK {
			log.RunLogger.Errorf("[config manager] get user_data error:%+v", err)
			time.Sleep(WaitTimeInterval * time.Second)
			continue
		}
		reqOK = true
	}
	if !reqOK {
		return "", "", "", fmt.Errorf("code %d or err %v", code, err)
	}
	var fleetIDStruct struct {
		FleetID        string `json:"fleet_id"`
		GatewayAddress string `json:"gateway_address"`
		ScalingGroupID string `json:"scaling_group_id"`
	}

	err = json.Unmarshal(buf, &fleetIDStruct)
	if err != nil {
		return "", "", "", err
	}

	return fleetIDStruct.FleetID, fleetIDStruct.GatewayAddress, fleetIDStruct.ScalingGroupID, nil
}

func getFleetIDAndGWAddrAndScalingGroupIDFromFile() (string, string, string, error) {
	buf, err := ioutil.ReadFile("C:/config.txt")
	if err != nil {
		log.RunLogger.Errorf("[config manager] getFleetIDAndGWAddrAndScalingGroupIDFromFile err:%v", err)
		return "", "", "", err
	}

	var fleetIDStruct struct {
		FleetID        string `json:"fleet_id"`
		GatewayAddress string `json:"gateway_address"`
		ScalingGroupID string `json:"scaling_group_id"`
	}

	err = json.Unmarshal(buf, &fleetIDStruct)
	if err != nil {
		return "", "", "", err
	}

	return fleetIDStruct.FleetID, fleetIDStruct.GatewayAddress, fleetIDStruct.ScalingGroupID, nil
}

func GetMetaDataConfig() (*Meta, error) {
	cli := clients.NewHttpsClientWithoutCerts()

	req, err := clients.NewRequest("GET",
		fmt.Sprintf("%s/openstack/latest/meta_data.json", config.Opts.CloudPlatformAddr),
		map[string][]string{}, nil)
	if err != nil {
		return nil, err
	}

	var code int
	var buf []byte
	var reqErr error
	reqOK := false
	// 获取metadata参数，失败每隔2s重试一次，最多10次
	for i := 0; i < MaxRetry && reqOK == false; i++ {
		code, buf, _, reqErr = clients.DoRequest(cli, req)
		if reqErr != nil || code != http.StatusOK {
			time.Sleep(WaitTimeInterval * time.Second)
			continue
		}
		reqOK = true
	}

	if !reqOK {
		return nil, fmt.Errorf("code %d or err %v, resp:%v", code, reqErr, string(buf))
	}

	metadata := &Metadata{}
	log.RunLogger.Errorf("[config manager] GetMetaDataConfig string:%v, buf:%v", string(buf), buf)
	err = json.Unmarshal(buf, &metadata)
	if err != nil || metadata == nil {
		return nil, err
	}

	meta := metadata.Meta
	log.RunLogger.Errorf("[config manager] GetMetaDataConfig success, meta:%+v", meta)
	return &meta, nil
}

func getInstanceID() (string, error) {
	cli := clients.NewHttpsClientWithoutCerts()

	req, err := clients.NewRequest("GET",
		fmt.Sprintf("%s/openstack/latest/meta_data.json", config.Opts.CloudPlatformAddr),
		map[string][]string{}, nil)
	if err != nil {
		return "", err
	}

	code, buf, _, err := clients.DoRequest(cli, req)
	if err != nil || code != http.StatusOK {
		return "", fmt.Errorf("code %d or err %v", code, err)
	}

	var instanceIDStruct struct {
		InstanceID string `json:"uuid"`
	}

	err = json.Unmarshal(buf, &instanceIDStruct)
	if err != nil {
		return "", err
	}

	return instanceIDStruct.InstanceID, nil
}

func getPublicIPOrPrivateIP(ipType string) (string, error) {
	cli := clients.NewHttpsClientWithoutCerts()

	var url string

	switch ipType {
	case publicIPType:
		url = fmt.Sprintf("%s/latest/meta-data/%s", config.Opts.CloudPlatformAddr, "public-ipv4")
	case privateIPType:
		url = fmt.Sprintf("%s/latest/meta-data/%s", config.Opts.CloudPlatformAddr, "local-ipv4")
	default:
		return "", fmt.Errorf("error ip type")
	}

	req, err := clients.NewRequest("GET", url, map[string][]string{}, nil)
	if err != nil {
		return "", err
	}

	code, buf, _, err := clients.DoRequest(cli, req)
	if err != nil || code != http.StatusOK {
		return "", fmt.Errorf("code %d or err %v", code, err)
	}

	return string(buf), nil
}

func GetConfigFromAppGateway(hostName string) (configFromAppG, error) {
	//send request to appG
	response, err := clients.GWClient.GetGetConfigFromAppGatewayByHostname(hostName)
	var resultStruct configFromAppG
	if err != nil {
		return resultStruct, err
	}

	resultStruct.FleetID = response.FleetID
	resultStruct.ScalingGroupID = response.ScalingGroupID
	resultStruct.InstanceID = response.InstanceID
	resultStruct.PublicIP = response.PublicIP
	resultStruct.PrivateIP = response.PrivateIP
	resultStruct.ProjectID = response.ProjectID

	return resultStruct, nil
}
